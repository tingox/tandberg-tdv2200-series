EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 12
Title "Tandberg TDV2200 schematics"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 4100 1000 1650 1500
U 604E65CC
F0 "Keyboard" 50
F1 "Keyboard.sch" 50
$EndSheet
$Sheet
S 2300 3000 1900 1800
U 604E6628
F0 "mainboard" 50
F1 "mainboard.sch" 50
$EndSheet
$Sheet
S 1000 5350 1750 1450
U 604E66A5
F0 "Power / Deflection 1" 50
F1 "power_deflection_1.sch" 50
$EndSheet
$Sheet
S 4050 5350 1750 1400
U 604E66DF
F0 "Power / Deflection 2" 50
F1 "power_deflection_2.sch" 50
$EndSheet
$Sheet
S 1050 1000 1650 1500
U 604E6710
F0 "Tube Board" 50
F1 "tube_board.sch" 50
$EndSheet
$Sheet
S 6400 1000 1750 1500
U 6060AAF5
F0 "V.24 interface adapter" 50
F1 "v24_interface_adapter.sch" 50
$EndSheet
$Sheet
S 6450 2950 1700 1800
U 619332C4
F0 "Current loop interface adapter" 50
F1 "current_loop.sch" 50
$EndSheet
$EndSCHEMATC
