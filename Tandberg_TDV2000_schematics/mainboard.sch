EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 12
Title "Mainboard"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 3150 1500 1750 1900
U 619A2A52
F0 "Mainboard - sheet 1" 50
F1 "mainboard_1.sch" 50
$EndSheet
$Sheet
S 6400 1500 1900 1900
U 619A2AA8
F0 "Mainboard - sheet 2" 50
F1 "mainboard_2.sch" 50
$EndSheet
$Sheet
S 3150 4700 1750 1600
U 619A2B20
F0 "Mainboard - sheet 3" 50
F1 "mainboard_3.sch" 50
$EndSheet
$Sheet
S 6450 4700 1900 1600
U 619A2C1F
F0 "Mainboard - sheet 4" 50
F1 "mainboard_4.sch" 50
$EndSheet
$EndSCHEMATC
