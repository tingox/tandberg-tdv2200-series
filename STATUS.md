# Status

* Location and Interconnection:
* Keyboard: schematic complete, error checking done.
* Mainboard: schematic complete, all sheets needs to be checked for errors.
* Power / Deflection 1: schematic complete, needs to be checked for errors.
* Power / Deflection 2: schematic complete, needs to be checked for errors.
* Tube board: schematic complete, error checking done.

Optional interface adapters
* V.24 interface adapter: schematic complete, error checking done.
* Current loop interface adapter: schematic complete, needs to be checked for errors.
