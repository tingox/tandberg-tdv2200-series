# Tandberg TDV 2200 series terminal

Schematics for the Tandberg TDV 2200 series terminal, re-created from a paper copy of the schematics.

Component designations follows the original schematic as closely as possible, to aid in faultfinding (component references are printed on the component
side of PCB's (printed circuit boards). This means that the schematics here deviates from KiCAD component naming conventions.