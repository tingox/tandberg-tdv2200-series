# Address map
The overall address map for the terminal isn't documented in manuals. From my own notes I have this

    0000H - 6000H	ROM and RAM on mainboard
    6000H - 8000H   EAROM and Character Generator
    8000H - FFFFH   Extra memory board or Test board

For details of the ROM and RAM area, see the Address Decoder section in [mainboard](./mainboard.md).

## I/O addresses
Since the IN and OUT instructions place the portnumber on both the low and the high half of the address bus, you get away with just decoding half of it.

    0000H  -CSIO (Chip Enable, Line Interface and Interrupt Logic)
    1000H  -CKBPR (Chip Enable, Keyboard and Printer)
    2000H  -CTIM (Chip Enable, Timer)
    3000H  -CCRT (Chip Enable, CRT Controller)
    4000H  -CDMA (Chip Enable, Direct Memory Access Controller)
    5000H  -SCE1 (Select Control Port 1)
    6000H  -SCE2 (Select Control Port 2)
    7000H  -ATRTE (Attribute Register Enable)

For details like registers within a specific chip (example: SIO, Timer, CRT Controller) look into the datasheet for the chip in question.