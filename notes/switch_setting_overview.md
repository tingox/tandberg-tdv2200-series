# Switch Setting Overview

## Introduction
There are several versions of the TDV 2200 terminal, and they again are used in different applications. A number of switches, straps and other controls have therefore been used to set each terminal up in order to function properly in each application.

A good knowledge of how each version is supposed to function is required to service them properly. Information about this is found in the Owner's Manual for each version of the terminal.

The following is an overview of the different jumpers, switches, controls and IC sockets in use when the terminal were set up. Detailed descriptions are given in the sections for each printed circuit board.

## Soft-switches
In addition to the mechanical devices used to control the operation of the terminals, there are a number of so called Soft-switches that can be altered from the keyboard and whose settings are stored in non-volatile memory.

The Soft-switches are divided into two or three groups, according to terminal version, and each group has its own menu:

- Convenience switches (Menu 1) are included for operator convenience and have no bearing on the function of the terminal.

Examples: Cursor type, Key click, Margin bell, Auto repeat

- Function switches (Menu 2) define the functional characteristics of the terminal.

Examples: Time-out, Bell (overrides the Margin Bell convenience switch)

- Communication switches (Menu 3) control the operation of the line and printer interfaces.

Examples: Simultaneous/Transparent, Echo, On-line

The Soft-switches are described in detail in the "Functional Specifications" section of the individual Owner's Manuals.

## Mainboard

| FUNCTION | PURPOSE | TYPE OF CONTROL |
| ---      | ---     | ---             |
| Memory   | Adapting to various types of memory packages | Jumpers, IC sockets, and EAROM programming |
| Character generator | Adapting to various types of ROM / PROM packages | Jumpers and IC socket |
| Dot 9    | Decidiing whether dot 9 is to be displayed or not | Jumper |
| Interfaces | Selecting internal / external transmit / receive clock signals | Jumpers |

## Options
There are a number of optional boards available for the TDV 2200 terminal. They are mounted on the Mainboard and most of them require some form of switch setting on the Mainboard or on the optional board itself. The type of control listed in the table below refer to the Mainboard; eventual controls on the optional boards are described in the service documentation for each individual board.

| OPTION                     | PURPOSE | TYPE OF CONTROL ON MAINBOARD |
| ---                        | ---     | ---                          |
| Video output               | Optional board; output for video monitor | Jumpers |
| Memory Module (Test board) | Optional board, additional memory capacity |None (Jumpers on the Memory module) |
| Dynamic RAM, 16 K or 32 K  | Optional board, additional memory capacity | None |
| Current loop adapter       | Optional board, communication | Jumper |
| V.11 interface adapter     | Optional board, communication | Jumper |
| V.24 interface adapter     | Optional board, communication | Jumper |

## Power / deflection 1

| FUNCTION | PURPOSE                    | TYPE OF CONTROL |
| ---      | ---                        | ---             |
| +5 V     | Adjusting the +5 V voltage | Potmeter |
| Horizontal phase | Adjusting the horizontal phase | Potmeter |
| Horizontal linearity | Adjusting the horizontal linearity | Potmeter |

## Power / deflection 2

| FUNCTION | PURPOSE                    | TYPE OF CONTROL |
| ---      | ---                        | ---             |
| Black level | Adjusting the black level (background) | Potmeter |
| Extra-high Tension | Adjusting the Extra-high Tension | Coil |
| Focus    | Adjusting focusing voltage | Potmeter |
| Horizontal linearity | Adjusting the horizontal linearity | Coil |
| Horizontal shift | Adjustying the horizontal shift | Potmeter |
| Horizontal shift select | Selecting left or right shift | Jumper |
| Vertical frequency | Adjusting vertical frequency | Potmeter |
| Vertical linearity | Adjusting vertical linearity | Potmeter |
| Quiescent current | Adjusting quiescent current | Potmeter |
| Width | Adjusting the picture width | Coil |


