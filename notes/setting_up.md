# Setting up

Jumpers on the Mainboard affect the following main functions:

| FUNCTION | PURPOSE |
|---               |---              |
| MEMORY | Adapting to various types of memory packages |
| CHARACTER GENERATOR | Adapting to various types of ROM / PROM packages |
| DOT 9 | Deciding whether dot 9 is to be displayed or not |
| INTERFACES | Selecting internal / external transmit / receive clock signals |
| VIDEO OUTPUT OPTION | Described in separate documentation for Video Output Option, publ. no. 5323 |

The location of the various jumpers are show in the component location drawing, section 6.2

## Memory Jumpers

NOTE: In addition to setting jumpers as described below, the contents of the EAROM must be set in accordance with the type of memory
circuit used. See EAROM programming in part 10 (Servicing) of the TDV 2200 Service Manual.

| Memory package type | Pos no. | Socket | Jumpers to be inserted |
|---                                  |---          |---         |---                                   |
| 16 K ROM / EPROM | U31 | 1 | SW1 |
|   | U32 | 2 | SW3 |
|   | U33 | 3 | SW22A |
|   | U34 | 4 | SW23A |
|   | U35 | 5 | SW26A |
|- 
| 64 K ROM / EPROM | U31 | 1 | SW2 |
|   | U32 | 2 | SW4 |
|- 
| 16 K RAM | U33 | 3 | SW22B |
|    | U34 | 4 | SW23B |
|    | U35 | 5 | SW26B |

## Character Generator Jumpers

| Package type in pos. U63 | SW11 | SW10 | SW16 | SW17 |
|---                                       |---       |---        |---        |---       |
| 16 K EPROM 2716 (Intel) | | I | O | O | I |
| 16 K ROM 68 A316E (Motorola) | X | X | X | X |
| 16 K EPROM 2516 (Texas) | I | O | O |  I  |
| 32 K EPROM 2732 (Intel) | O | I | O | I |
| 32 K EPROM 2532 (Texas) | I | O | I | O |
| 32 K ROM 68 A332 (Motorola) | X | X | I | O |

I = inserted, O = open, X = don't care

## Dot 9

Dot 9 not displayed: Insert jumper 13

## Interfaces

### Channel A

| Clock signal distibution | Jumpers to be inserted |
|---                                    |---                                   |
| External Receive clock | none |
| External Transmit clock | none |
| Internal Receive clock | SW9 |
| Internal Transmit clock | SW8 |

Standard setting: enither SW8 nor SW9 inserted

Connector W13 shall have a jumper between pins 2 and 3 when Channel A optional interface is not connected.

### Channel B

| Clock signal distibution | Jumpers to be inserted |
|---                                    |---                                   |
| Internal Receive Clock to host computer | SW18 |
| Internal Transmit Clock to host computer | SW19 |
| External Receive Clock from host computer | SW21 |
| External Transmit Clock from host computer | SW20 |

Standard setting: SW20 and SW21 inserted.

Connector W11 shall have a jumper between pins 2 and 3 when Channel B optional interface is not connected.

### Printer Optional Interface

Connector W14 shall have a jumper between two pins if no printer is connected.
The strapping depends on the revision level of the Mainboard:

    Mainboard revision level up to 10      : pins 1 and 3
    Mainboard revision level 11 and higher : pins 2 and 3

### Video Output Option

Setting of SW25, SW27 and SW30 for the Video Output Option is described in a spearate publication, "Video Output", publ. no. 5323.
