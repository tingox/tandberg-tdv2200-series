# Mainboard
The Mainboard has been produced in two versions.

Early version: revision level 10 - 14 (Dynamic RAM)

New version  : revision level 15 and higher (Static RAM)

The schematics and description here are for the Mainboard revision level 15.

## Description

### CPU
The CPU controls all the activities within the terminal. It is governed by a crystal clock with a frequency of 4.9152 MHz.

### Power Up/Down and Reset
The CPU is kept reset for about 250 ms after power-up to allow the +5 V supply to syabilize. The CPU is reset 45 ms after power-down. Power down is represented by LOW MAINS which occurs when
the mains voltage drops below a set value. Pressing the RESET SWITCH resets the CPU immediately.

### CPU Buffer
The multiplexed address/data-bus and upper byte address is buffered and latched onto the ABUS.

### Address decoder
The address range is divided in two areas. The Lower address area from 0000H to 5FFFH is decoded into siz different signal enabling the various chips of the CPU memory

| SIGNAL | ADRESS RANGE | DESCRIPTION |
|---     |---    |---    |
| CES0 | 0000H - 1FFFH | 2/4/8 Kbyte ROM/EPROM |
| CES1 | 2000H - 3FFFH | 2/4/8 Kbyte ROM/EPROM |
| CES2 | 4000H - 47FFH | 2 Kbyte EPROM or RAM |
| CES3 | 4800H - 4FFFH | 2 Kbyte EPROM or RAM |
| CES4 | 5000H - 57FFH | 2 Kbyte EPROM or RAM |
| CES5 | 5800H - 5FFFH | 2 Kbyte RAM |

Within the address ranges listed above, the Display Memory, the Non-Volatile Memory and external memory are inhibited by IBUIN being true.
These memories can be accessed by addresses above 6000H when IBUIN is false. See [address map](./address_map.md) for the overall address map.

### Data Bus Buffer
The bi-directional Data Bus Buffer is disabled by the IBUIN signal. The flow direction is determined by the state of READ, S1 and ALE signals.

### CPU Memory
Siz memory chips of different types can be plugged into sockets, out of which two are assigned for PROM only, one for system RAM only and three sockets
for eith EPROM or RAM depending on strapping on the board.
Each is assigned a defined address area related to a particular chp enable pulse (CES0-CES5) from the Address Decoder, see Adress Decoder section.
Total memory capacity: 24 Kbytes.

### Non-Volatile Memory
The Non-Volatile Memory is an EAROM that maintains its contents when the power is turned off. The data is organised in 1024 by 4 bits; 512 bytes.
Each byte consists of two nibbles (4 bits), so read or write operations must be performed in two steps. The READ, WRITE and ERASE modes are controlled
by the A10 and SI lines.

The Non-Volatile memory is accessed by adresses above 6000H when enabled by CNVD from the Address Decoder, WR from the CPU, and NVSEL from the Control Port.
The WAIT signal is returned to the CPU while the Non-Volatile Memory is being accessed.

This memory requires -12 V and -30 V supply voltage in addition to +5 V.
Capacity: 512 bytes used for storing Soft-switches and PUSH-key strings.

### Interrupt Logic
The interrupt signals for power failure (PFAIL), vertical sync (VSYNC) and from the Keyboard (KBINT), are handled directly by the CPU, here mentioned
in descending priority order.

The interrupt on the fourth priority level is shared by the Line Interface (SIOINT) and the Printer Interface (PRINT) via the Interrupt Logic. The relative prority ranking of these two is under software
control. Either one of the signals SIOINT or PRINT will generate an interrupt request (INTR) to the CPU which will respond with an INTA when ready to handle the interrupt.
INTA will then allow the CPU to read the interrupt identification via the D-bus.

During an interrupt from the Line Interface (SIOINT), CSIO will prolong the machine cycle by one clock period (407 ns) to allow sufficient time for a read or write operation in the SIO.
The WAIT signal will have a similar effect allowing sufficient time for a read or write operation in the Non-Volatile Memory.

### I/O Logic
Decodes the address bits A12 - A15 to establish the following enable signals:
| SIGNAL | DESCRIPTION |
|---         |---  |
| CSIO    | Chip Enable, Line Interface and Interrupt Logic |
| CKBPR | Chip Enable, Keyboard and Printer |
| CTIM    | Chip Enable, Timer |
| CCRT    | Chip Enable, CRT controller |
| CDMA   | Chip Enable, Direct Memory Access Controller |
| SCE1    | Select Control Port 1 |
| SCE2    | Select Control Port 2 |
| ATRTE  | Attribute Register Enable |

### Timing Logic
Based on an oscillator frequency of 2.4576 MHz (CLKH) the Timing Logic derives the clock signals CIA and CIB used by channels A and B, respectively in the Line Interface. Furthermore it generates 
the clock PRCLK for the Printer Interface, and KBCLK and LACLK for the Keyboard Interface.

The keyboard clock signals are derived by counting down the CLKH at fixed rates. The CIA, CIB and PRCLK can, however, by programming of the timer be set for frequencies in the range 800 Hz to
614.4 kHz corresponding to baud rates between 50 and 38400.

The programming is accomplished via the D-bus when the timer is accessed by CTIM, and the appropriate register is specified by the address bits A0-A1.

### Keyboard Interface
Converts serial data from the Keyboard (RxKB) to parallel data clocked by KBCLK (2400 baud) and generates an interrupt request (KBINT) for each keyboard entry. Converts parallel data from the CPU
to serial data (TxRB) to be transmitted to the Keyboard clocked by LACLK (2400 baud).
Furthermore, the CPU can read status signal from the Keyboard or send commands to it when the chip enable signal CKBPR is true.

### Printer Interface
The parallel/series conversion of data to and from the printer is done by and asynchronous ACIA (MC 6850). It is initialized and loaded from the CPU.
If the printer is busy, the interrupt line (PRINT) goes low.

### Line Interface
The central circuit in the Line Interface is a communications controller (in the following referred to as a SIO) which is programmed and controlled from the CPU.
The standard version of the terminal is equipped with a one-channel SIO used for channel A. A two-channel SIO is optional.

The SIO normally operates on the system clock (2.46 MHz) from the Timing Logic. However, external clock operationcan be selected for channel Awith the signal ESCA via the control port if the
emulator has the ability to use external clock. Selection of V.11 or V.24 interface depends on the state of the V24EN signal from the control port.

Communication with the CPU takes place on interrupt by the SIOINT.

During power-up a loop-back test carried out by the CPU is initiated by the BAC signal from the Control Port. During the test the transmit line (TxD) and the receive line (RxD) are interconnected.

### Status Port
The CPU enables the Status Port with an SCE1 via the I/O Logic and an RD strobe to read status signals associated with the line interface and signals used for interanl testing.

### Control Ports 1 and 2
The CPU enables Control Ports 1 and 2  by an SCE1 or SCE2 signal, respectively from the I/O-logic. It then loads the port via the DB-bus to set the various outputs as required and thus selects one
or more of the functions or operational modes listed below.

| SIGNAL | DESCRIPTION | COMMENT |
|---          |---                    |---                |
| C0, C1  | Memory map control | Factory preset at 00 fro the standard version. |
| NVSEL  | Non-volatile Memory select | |
| V24EN  | V.24 Interface enable | |
| ESCA    | External clock select | |
| SSE      | Modem speed select | |
| SETSEL | Select Display Memory for signature analysis | |
| DISBRD | Disabling Display Memory Buffer read by inhibiting CHRRD from the Display Memory Access logic | |
| ATRW | Attribute write. Controls the writing of attribute codes into the Display RAM | |
| CHRW | Character write. Controls the writing of character codes into the Character RAM | |
| BCKCUR | Block cursor. To Attribute Generator | |
| VOF        | Video off. To Attribute Generator | |
| CEN        | Cursor enable. To Attribute Generator | |
| SELDM | Select Display Memory. To Display Memory Access logic | |
| BAC | Loop-back test for Line Interface | |
| DISSEL | Disselect Display Memory. To Display Memory Access logic | |

### CRT Controller & Sync Buffer
Contains 16 programmable registers that can be addressedbu A0 - A3. The programming is accomplished by the CPU via the D-bus, when CCRT is active, and affects the timing and the format of the
output signal pattern. When enabled by CCRT, the CRT Controller is blocked by QC which is in synchronism with the character clock.

The following output signals are available:
| SIGNAL | DESCRIPTION |
|---          |---                    |
| RS0 - RS3 | Raster scan bits representing the scan number in each character row. Runs through the sequence 0 - 13 (decimal) repetitively and is used as scan address for the Character Generator. The raster scan bits are decoded in the Attribute Generator to enable underline mode (UNLIEN) |
| HSYNC, VSYNC, BLANK | Timing pulses locked to the character, row and frame rates. |
| H0 - H6, DR0 - DR4 | Address bits representing the character position on the screen. Runs through the sequence 0-2000 (25 x 80) (decimal) to provide the read-out address for the Display Memory each time the display is updated. |
| CURSOR | The address on the HD-bus is continuously compared against the current cursor address which is contained in an internal register in the CRT Controller. The cursor signal is generated when these two addresses are equal and will thus determine the cursor position on the display. The cursor address register is automatically updated by the CPU which can also read the register. |

### CRT Clock
The CRT Clock Circuit provides the timing for the CRT functions. The basic frequency is 20.2608 MHz which is also the video dot frequency, CRTCLK, clock period 43.36 ns. A 4 bit counter generates the
CHRLD and QC signal both with a clock period of 9 x 49.36 = 444.2 ns, but with a different duty cycle. See timing diagram, section 5.1.

The CEDM signal (444.2 ns clock period) is the basic chip enable signal to the Display RAM. BLANK 1 is synchronized with CECLK and deactivates the CEDM signal.

When the terminal is set to elongated mode, every dot in a character is displayed twice in a row. U79 along with the multiplexer U87 sets the DOTCLK to half the speed (2 x 49.23 = 98.72 ns clock
period) when this feature is enabled by ATTR0.

### Display Address Multiplexer
When SELECT is false, the HD-bus is connected to the Display Memory and will provide the read-out address for updating of the display.
When SELECT is true, the CPU is given access to the Display Memory via the A-bus.

### Display Memory
See timing diagram, section 5.2.
The Display Memory consists of one Characther RAM and one attribute RAM.
Each character position on the display corresponds to one particular location (cell) in the Character RAM as well as in the Attribute RAM.
Reading takes place simultaneously from the character and attribute sections when enabled by DMCE and DMEMOE. Writing into the memory also
requires DMCE to be true. Furthermore, CHRWE must be true for writing into the Character RA and ATRWE must be true for writing into the Attribute RAM.

The address for reading as well as for writing is applied via the Display Address Multiplexer.

### Display Data Buffer
The CPU can access the Character RAM as well as the Attribute RAM but not at the same time. A bidirectional buffer gives access to the character section in write and read modes
whereas data to and from the attribute section is transferred via separate data latches for the write and read operations.

When the CPU wants to write into the Display Memory, the CPU first loads the attribute code into the Write Latch while WR and ATRTE arre true. Then, while the CPU applies the address via the
Address Multiplexer, it sends the character code via the DB-bus and the Display Data Buffer which is enabled in the write direction by DBBWE. At the same time DBBWE copies the attribute code from
the latch into the memory.

In the read mode the attribute code from the addressed memory location is clocked into the Read  Latch by ATRST. When RD and ATRTE become true, this code can be read by the CPU via the DB-bus.
When CHRRD goes true, it enables the Character Buffer in the read direction, and the character code from the addressed memory location becomes available on the data bus.

### Display Memory Access Logic
See timing diagram, section 5.3.
Provides the signals that give the CPU access to the Display Memoryin the horizontal and vertical flyback periods during which the CRT Controller does not access the memory.

Based on the signals HSYNC and BLANK from the CRT Controller the Access Logic generates a single access window pulse (ACCWDW) which is 9.3 us long during HSYNC and 25 pulses of 50 us duration
during VSYNC. When ACCWDW is true, SELECT will go true after the first ALE pulse from the CPU. If a request from the CPU arrives while this situation exists, the CPU will be granted access to the 
Display Memory, and ACSUCS will be sent to the CPU to acknowledge the access.

The Display Memory Access Logic also provides the timing signals that control the Display Memory and the Display Data Buffer.

### Character Generator
See timing diagram, section 5.4.
The dot pattern matrixes for all display characters are stored in a PROM. Each matrix constitutes nine 8-bit words representing the dot pattern in the 9 scans needed to display one character. When a
dot pattern word is accessed, it will be parallel loaded into a shift register from which it is shifted out serially at the dot clock rate (DOTCLK) to become the raw video signal (DOTS).

The nine dot pattern words in one character matric takes up nine consecutive locations in the Character PROM. To load the eight bits in one particular dot pattern word into the shift register, the PROM
requires an address in two parts; one specifying the character (the character code BC0-BC7 from the Display Memory), and one specifying the scan number in the character row (the Raster Scan bits
RS0 - RS3 from the CRT controller).
The character code is updated at the character rate (CHRLD) whereas the scan address remains unchanged throughout each complete scan and runs through the seuence 0-13 during each character row.
The 9th bit is represented byt bit 8 being loaded into the serial input and then shifted through until it appears at the output during the 9th interval. Since bit 8 usually is 0 for all characters, bit 9 will
also be 0 providing an inter-character space.

### Attribute Generator
Provides the signals that affects thevideo signal in such a way that characters, cursor and underline can be presented in vrious modes on the display:

    ELONG  elongateded mode
    INVERT  inverse video
    INVIS     invisible (video off)
    LOWINT  low intensity

Each one of these signals alone and the combinations of them (except ELONG) give a total of 16 different display modes. the combinations are store in the Attribute PROM and can be accessed by a 
9-bit address consisting of the 4-bit attribute code contained in the Display Memory and the remaining 5 bits represented by signals gernerated logically or set by a CPU command via a control port.

The 4-bit address representing the basic display mode is modified by the other 5 bits in order to access the appropriate attribute signal combination according to whether the character, the cursor or
the underline is being scanned. The combinations can either be simultaneous or sequential. If for instance the attribute code specifies low intensity blink mode, the two PROM locations representing
low intensity and normal intensity will be accessed alternately at one sixteenth of the VSYNC rate (3 Hz).

In the cursor position (CURSOR true) the state of BCKCUR will dtermine whether the cursor is displayed in all 14 scans (block cursor) or in the 11th scan only (line cursor).

### Video Track
The DOTS signal from the Character Generator is modified by the signals INVERT, INVIS and LOWINT from the Attribute Generator resulting in the three output signals VIDEO1, VIDEO2 and VIDEO3.
The combination of these three output signals determines how characters, cursor and underline are presented on the display.

INVERT causes an inversion of the dot signal resulting in inverse video. INVIS stops the dot stream resulting in invisible video.
LOWINT deactivates VIDEO3 and causes low intensity. Dot 9 in the Character Matrix which represents the spacing between characters, is inhibited by the CHRLD pulse provided the strap SW13 is
connected.

The Video Track Logic continuously examines three consecutive dot pulse intervals to find out whether the dot being displayed is single, or whether it belongs to an uninterrupted sequence of two or
more dots. If it is a single dot, VIDEO1, VIDEO2 and VIDEO3 are all active resulting in increased intensity. Otherwise, only VIDEO1 and VIDEO3 are active giving normal intensity.

### Video Amplifier
The output from Video Track and Blanking is amplified in the Video Amplifier.

## Signal mnemonics

| MNEMONIC    | FUNCTION |
| ---         | ---      |
| AB0, 1, 2, 3 | Attribute bit 0, 1, 2, 3 |
| ABUS | Address bus |
| ACCWDW | Access window |
| ACWCLK | Access window clock |
| ACSUCS | CPU succeeded in access to display memory |
| ACWDCC | Access window clear |
| ALE    | Address latch enable |
| ATRCK  | Attribute clock |
| ATRST  | Attribute strobe |
| ATRTE  | Attribute register enable - write and read latches |
| ATRW   | Attribute Write, Controls the writing of attributes in the display memory |
|        | 0= Inhibit attribute write |
|        | 1= Enable attribute write |
| ATRWE  | Attribute write enable |
| ATTR0  | Attribute mode 0 enables elongated |
| BAC    | Loopback on serial interface |
|        | 0= normal operation |
|        | 1= Lopp back |
| BKCUR  | Block cursor, Select cursor representation |
|        | 0= Steady underline |
|        | 1= Steady block |
| BLANK  | Blanking signal |
| BLANK 1 | Blanking clocked with character clock | 
| BLANK 2 | Blanking delayed one character clock period |
| BLINK  | Blink |
| BLKFRQ | Blink frequency - 3.12 Hz |
| C0, C1 | Memory map control - factory preset at 00 for the standard version. |
| CAR    | Carrier Detect, Status of modem signal |
| CB 0 to 7 | Character bit 0 to 7 |
| CBUS   | Control bus |
| CCRT   | Chip enable CRT controller |
| CDMA   | Chip enable direct memory access controller |
| CECLK  | Character enable clock - 2.251 MHz (444.2 ns) |
| CEDM   | Chip enable clock for display memory - 2.251 MHz |
| CEN    | Cursor enable, the cursor switches on and off |
|        | 0= cursor off |
|        | 1= cursor on |
| CES0 to 5 | Enable chip in socket pos 0 to 5 |
| CHRLD  | Loads character into character register- 2.251 MHz |
| CHRRD  | Character read in display RAM |
| CHRW   | Character write, controls the writing of characters in the display RAM |
|        | 0= Inhibit character write |
|        | 1= Enable character write |
| CI     | Calling Indicator, status of modem signal |
| CIA    | Internal clock to SIO Channel A |
| CIB    | Internal clock to SIO Channel B |
| CKBPR  | Chip enable keyboard and printer |
| CLKH   | Timer input clock |
| CNVD   | Select non-volatile and display RAM |
| CRTCLK | CRT clock 20.2608 MHz (49.36 ns) |
| CSIO   | Chip enable SIO |
| CTIM   | Chip enable timer |
| CTS    | Clear to send Channel A |
| CTSB   | Clear to send Channel B |
| CURSEN | Cursor enabled |
| CURSOR | Cursor status |
| DBBUS  | Buffered data bus |
| DBBWE  | Enable write data from DBBUS into display RAM |
| DBUS   | Data bus |
| DISBRD | Disable display memory buffer read |
|        | 0= Display memory buffer read operation enabled |
|        | 1= Display memory buffer read operation disabled |
| DISSEL | Disselect display memory |
|        | 0= display memory disselected from the CPU |
|        | 1= display memory in normal operation |
| DLHSY  | Delayed HSYNC |
| DMARQA | DMA interrupt from SIO Channel A |
| DMARQB | DMA interrupt from SIO Channel B |
| DMCE   | Data memoru chip enable |
| DMEMOE | Display memory output enable |
| DOT 0 to 9 | Dot number 0 to 9 |
| DOTCLK | Serial conversion dot clock, 20.268 MHz except in elongated mode when the clock rate is divided by two |
| DR0 - DR4 | Data row address bits (text line) |
| DSR    | Data signal ready Channel A |
| DSRB   | Data signal ready Channel B |
| DTR1   | Data terminal ready Channel A |
| DTRB   | Data terminal ready Channel B |
| ECSA   | External clock select on SIO channel A |
|        | 0= Internal Clock |
|        | 1= External Clock |
| ELG    | Elongated display mode, each dot displayed twice |
| H0 - H5 | Horizontal address bits (character position) |
| HOLD   | Hold request to CPU |
| HLDA   | Hold acknowledge from CPU |
| HSYNC  | Horizontal synchronization. Derived from HSYNCO |
| HSYNCO | Horizontal sync from CRT controller |
| IBUIN  | Inhibit buffer in |
| INSP   | Input Spare |
| INTA   | Interrupt acknowledge |
| INTCE  | Interrupt chip enable |
| INTDIS | Interrupt disable | 
| INTR   | Interrupt request |
| INVDO  | Inverse video |
| INVERT | Inverse video (attribute) |
| INVIS  | Invisible video (attribute) |
| IORQ   | Input/output request |
| IRBUS  | Interrupt bus |
| KBCLK  | Keyboard data clock |
| KBINT  | Interrupt from keyboard |
| LACLK  | Keyboard lamps clock |
| LC0 - LC7 | Loaded character bit 0 to 7 |
| LOW MAINS | Power supply below a set value |
| LOWINT | Low intensity (attribute) |
| MBRDY  | Mainboard ready |
| MCLEAR | Master clear |
| MOBAC  | Modem signals loop back |
| NVSEL  | Select non-volatile memory |
|        | 0= display memory selected |
|        | 1= Non-volatile memory selected |
| PFAIL  | Power failure |
| PRCLK  | Printer clock, programmable from 800 Hz to 614.4 kHz |
| PRINT  | Interrupt from printer |
| QC     | System clock CRT controller - 2.251 MHz 444.2 ns |
| RAMWR  | RAM write |
| RD     | Read (generated by the CPU) |
| RDY    | CPU ready |
| REQ    | CPU requests access to display memory |
| RESET  | Resets the CPU |
| RSB    | Raster scan bus |
| RS0 - RS3 | Raster scan bits |
| RSSW   | Reset switch - signal from manual reset switch |
| RTS1   | Request to send, Channel A |
| RTSB   | Request to send, Channel B |
| RxC1   | Receive clock, Channel A |
| RxCB   | Receive clock, Channel B |
| RxCV   | External receiver clokc - supplied by host computer |
| RxD    | Receive data, Channel A |
| RxDB   | Receive data, Channel B |
| RXDV   | Receive data (V.11 / V.24) |
| RxPR1  | Receive printer |
| S1     | Status signal 1 from CPU |
| SCE1   | Select control port 1 |
| SCE2   | Select control and status port 2 |
| SELDM  | Select Display Memory, Selects the display memory to be totally under CPU control |
|        | 0= display memory selected |
|        | 1= display memory in normal operation |
| SELECT | Select display memory status signal |
| SETSEL | Select display memory for signature analysis |
|        | 0= Display memory selected |
|        | 1= Display memory in normal operation |
| SIOINT | Interrupt from SIO |
| SRLD   | Shiftregister load (character generator) |
| SSE    | Modem speed select |
|        | 0= CT111 on |
|        | 1= CT111 off |
| TXDB   | Transmit data Channel B |
| TXDV   | Transmit data (V.11 / V.24) |
| TxC1   | Transmit clock Channel A |
| TxCB   | Transmitter clock Channel B | 
| TxCV   | External transmitter clock |
| TxD    | Transmit data Channel A |
| UNDLIN | Underline |
| UNLIEN | Underline line |
| V24EN  | Enable V.24 interface signals |
|        | 0= V.11 line interface enabled |
|        | 1= V.24 line interface enabled |
| VIDEO1 | Normal video |
| VIDEO2 | Single dot video |
| VIDEO3 | Low intensity video |
| VOF    | Video off, the picture is turned on and off by this bit |
|        | 0= video on |
|        | 1= video off |
| VON    | Video on |
| VSYNC  | Vertical synchronization, also used as: Interrupt from timer every vertical flyback on screen |
| VSYNCO | Vertical sync for CRT controller |
| WAIT   | EAROM wait cycle signal - Non-volatile memory |

## Sheet 1
* U31 (MK37013P) ROM - contains "Primitives I"
* U32 (2764)   EPROM - contains "Primitives II"
* U33 (2716)   EPROM - contains "Emulator"

Both U34 and U35 are labeled as "not in use" in my notes.

## Sheet 2
* KBCLK (153.6 kHz) is receive clock for data from keyboard, at 9600 baud
* LACLK (38.4 kHz) is transmit clock for data to keyboard, at 2400 baud

## Sheet 3
* Note: the pull up resistor for U73 pin 6,7 and 10 is not labeled. I just picked  (randomly) pin 5 on RP6.
* Note: the pull up resistor for U73 pin 3,4,5 is not labeled. I picked (randomly) pin 8 on RP6.

## Sheet 4
* Note: the input for U75, pin 11 is a signal labeled "DOTCKA", coming from mainboard sheet 3, coordinates F1 ("3-F1").
  The only relevant signal around there on 3-F1 labeled as going to sheet 4 is "DOTCLK". So use "DOTCLK" instead.
* Note: RP8 and RP9 has no pin designations on the original schematic, I have chosen random pins when connecting them to
  pins on U20.
* Note: W15 is missing from the parts list, but based on the component location drawing, this is probably the ground
  connector for the chassis.