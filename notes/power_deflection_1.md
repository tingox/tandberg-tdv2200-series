# Power / Deflection 1

## Signal mnemonics

| MNEMONIC    | FUNCTION |
| ---         | ---      |
| BLANKING |	Blanking pulse for the CRT via Q51 on the Mainboard |
| CATHODE  |	Video signal from Mainboard (derived from BLANKING in Q51) |
| COL Q14  |	Collector of Q14. Drive signal for the horizontal deflection transformer on P/D2 (Power / Deflection 2) |
| CRT HEAT |	Filament voltage for the CRT |
| EHT	|	High tension (16 kV) for CRT |
| FOCUS |	Voltage for the CRT focusing dectrode |
| G1 |		Blanking signal from Mainboard (derived from BLANKING) |
| G2 |		Voltage for grid 2 in the CRT |
| H SYNC |	Horizontal sync |
| H. DEFL. A |	Horizontal deflection yoke |
| H. DEFL. B |	Horizontal deflection yoke |
| LOW MAINS |	Signal to Mainboard when mains voltage drop below a set limit |
| V SYNC |	Vertical sync (from Mainboard) |
| V. DEFL. A |	Vertical deflection yoke |
| V. DEFL. B |	Vertical deflection yoke |

## Adjustments

R16 (h. freq.) - adjust to 20.1 kHz to keep picture straight (not leaning to one or the other side).

R20 (h. phase) - adjust the logical middle line so it is physically middle on the screen.
