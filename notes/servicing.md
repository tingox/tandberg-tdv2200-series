# Servicing

[[_TOC_]]

## Introduction
The TDV 2200 needs no preventive maintenance except for keeping the display screen and the keyboard reasonably clean.
Hence, the service is limited to fault finding and repairing.

Field service implies locating the fault to a particular board or any other major unit that can quickly be  replaced by a spare one. Once the fault has been located, removal of the faulty unit and replacement with a spare one is simple and usually requires no further explanation. However, although the new board is compatible with the one to be replaced, it may have to be set up in accordance with the detailed setting-up procedures given in the section for the board in question.

## Service equipment
In addition to standard tools and a digital voltmeter, the service engineer should have the following items available for field service use:

### Dummy plugs
To be plugged into the interface connector for testing of the terminal in local mode.

- Plug for  Channel A V.24
   - 25-pin female Cannon with the following pins strapped: 2 - 3, 4 - 5, 6 - 8 - 20

- Plug for Channel A Current loop
   - 25-pin female Cannon with the following pins strapped: 10 - 23, 12 - 24

- Plug for Channel A V.11
   - 15-pin male Cannon with the following pins strapped: 2 - 4, 3 - 5

- Plug for printer
   - 9-pin male Cannon with the following pins strapped: 2 - 4, 7 - 9

### Test board
To be plugged into the Mainboard. See separate description.

### Hot air blower (hair drier)
Used to heat component areas to provoke a periodic fault to appear.

### Cooling spray
Used to cool component arease to provoke a periodic fault to appear.

## Troubleshooting
We assume that troubleshooting is carried out by an experienced service engineer who is able to utilize the functional description in the Service Manual to locate faults. Furthermore, we introduce three aids:

1. Troubleshooting hints
   Based on experience obtained so far by our own service department. See separate [troubleshooting hints](#troubleshooting-hints) description.

2. Self test
   A test sequence is carried out automatically every time the terminal is switched on.
   An error code is displayed if the terminal fails in one of the tests. See section [Self test](#self-test).

3. Test board
   This is a strongly recommended aid for every service engineer.
   When the board is plugged in, you can choose from a number of teste that will be carried out at your command.
   See [Test board](#test-board-1) section.

### Troubleshooting hints
The table below is based on experience aquired by our service department but does not pretend to be comprehensive neither with respect to possible faults nor possible causes.
| SYMPTOM | POSSIBLE CAUSE | REMARKS |
| ---     | ---            | ---     |
| - no cursor<br>- double cursor<br>- Text jitter<br>- Cursor movements out of control<br>- Jerky roll<br>- Writes in the wrong line<br>- General video faults | CRT controller (U50) or Attribute RAM (U55) on Mainboard    | CRT controller can be damaged by flash-over in the picture tube. Check that the CRT-clock is ok (20.2608 MHz). Absence of cursor may also have other causes such as faulty U23. |
| - Blank screen (no video)<br>- Display compressed vertically | U8, U12, U18, U23 or U26 on Mainboard| These are only some of the possible causes |
| Arbitrary characters displayed over the entire screen or part of it at power-up | Display RAM (U51, U52), U18 or U26 on Mainboard | U18 pin 1 may be floating. Defective U23 will give the same symptom. |
| Wrong characters displayed in on-line or no characters at all | U11, U14, U16, U20 or U29 on Mainboard | The trouble usually appears 5-15 minutes after power-up. If the terminal does not operate on-line, check signal V24EN on U29 pin 9. If floating, replace U29. Check if current flows in the loop from the extra winding on the transfgormer on Power / Deflection 2. Check that pins 2 and 3 on W14, Mainboard are strapped together when using current loop. When using V.24 adapter, pins 2 and 3 on W13, Mainboard must be strapped. |
| The terminal is completely dead (no lamps on the keyboard are lit) | Fuses blown on Power / Deflection 1, Mains cable unplugged, CR12, CR15 or CR21 on Power / Deflection 1 |  |
| The power supply tries to start but fails (a tapping sound can be heard) | Q14 on Power / Deflection | or CR2 and CR3 on Power / Deflection 2 | Alsoi check the diodes listed under the preceding symptom) |
| "ERROR 14" is displayed | Contents in EAROM is incorrect for the existing layout | Insert Test board and correct the contents of the EAROM (see [EAROM programming](#earom-programming)). |
| "ERROR 1A" is displayed | Contents of all locations in EAROM is FF (hex) | Insert Test board and correct the contents of the EAROM (see [EAROM programming](#earom-programming)). If no success, the EAROM is defective. |
| "ERROR 19" is displayed | Contents in locations 50 to 60 (hex) of the EAROM does not agree with the existing memory configuration | Insert Test board and correct the contents of the EAROM (see [EAROM programming](#earom-programming)). |
| "EAROM Read/Write Error - Call the system operator" is displayed | Defective EAROM | Appears when you try to make a permanent change in the contents of the EAROM by means of the menus. |
| Blocked terminal (accepts no input) | Contents in EAROM from 00 (hex) to 0F (hex) is altered and has caused system clock change from ASY to ISO. | Insert Test board and select test no. 7 on the Test board menu. Type VS and write 00 in address 00, 1A in address 01, 17 in address 02 and 00 in the rest of the addresses up to 4F (hex). This procedure will open the terminal and you can enter the ordinary Soft-switch menu. |

### Self test
The Self Test is a test sequence stored in the primitives of the terminal. It is automatically carried out immediately after power-up.

Since the Self Test uses the terminal's CPU it is obvious that certain malfunctions such as processor defects, absence of internal system clock,data bus and address bus defects, and
to a certain extent defects of the primitives PROM will prevent a successful execution of the test scheme.
For the RAM/ROM/PROM tests it is also required that the processor is able to read the contents of the EAROM.

The Self Test program furthermore requires that the data describing the memory configuration (sizes of ROMs/PROMs in the various positions) is correctly programmed into the EAROM.
If changes in the memory configuration are required after delivery, the contents of the non-volatile memory must be altered accordingly by means of the Test board (see [EAROM programming](#earom-programming)).

The following test are carried out in Self Test:
- Checksum test of all ROMs/PROMs.
- RAM Pattern test of all RAMs except the system RAM.
- RAM Pattern test of the Disply Memory and the Attribute Memory.
- Asynchronous Communication test of the SIO channel A.
- Keyboard Memory test and keyboard Communication test.

When a test fails, one of the error codes listed in the follwing table is displayed on the screen.

| code | description |
| --- | --- |
| 02 | The test of the RAM chip with ref. no. 2 on the Mainboard failed |
| 03 | The test of the RAM chip with ref. no. 3 on the Mainboard failed |
| 04 | The test of the RAM chip with ref. no. 4 on the Mainboard failed |
| 05 | The test of the RAM chip with ref. no. 5 on the Mainboard failed |
| 06 | The test of the SYSTEM RAM with ref. no. 6 on the Mainboard failed. This code should only be retrieved from the power up test. The self test is not supposed to test this area, and if so, there must be a configuration error. |
| 07 | The test of the RAM chip with ref. no. 7 on the Memory Module or the dynamic RAM board failed |
| 08 | The test of the RAM chip with ref. no. 8 on the Memory Module or the dynamic RAM board failed |
| 09 | The test of the RAM chip with ref. no. 9 on the Memory Module failed |
| 0A | The test of the RAM chip with ref. no. 10 on the Memory Module failed |
| 0B | The test of the RAM chip with ref. no. 11 on the Memory Module failed |
| 0C | The test of the RAM chip with ref. no. 12 on the Memory Module failed |
| 0D | The test of the RAM chip with ref. no. 13 on the Memory Module failed |
| 0E | The test of the RAM chip with ref. no. 14 on the Memory Module failed |
| 0F | The display memory failed during the pattern test |
| 11 | The attribute memory failed during the pattern test |
| 12 | The SIO failed to report the expected status word |
| 13 | The SIO data was not correct |
| 14 | The SIO did not give interrupt |
| 15 | The keyboard ACIA failed to report the expected status word |
| 16 | The keyboard did not report the result of the internal Self Test |
| 17 | The keyboard reported error in the Checksum test |
| 18 | The keyboard reported error inthe RAM test |
| 19 | The accumulated checksum for all ROMs/PROMs was not correct |
| 1A | The memory configuration in the non-volatile memory is invalid |

### Test board
The Test board is a Memory Module containing the Test Monitor program described in publ. no. 5317 "TDV 2200 Test Monitor Specifications".
When the board is connected to connector W10 on the Mainboard, or to the Dynamic RAM board or Memory Module, the Test Monitor program will take
command of the terminal.
The self test normally carried out after power-up is then deleted and the various Soft-switches are set by the Test Monitor in the RAM copy
without disturbing the default setting in the non-volatile memory.

#### Operating Conditions
For the Test Monitor to operate the following components in the terminal must work: CPU, Address Bus, Data Bus, Primitives (correct version), System RAM and Video.

#### Test Facilities
The Test board continously tests itself and displays the following message if a malfunction is detected:

    TEST ABORT    ERROR TEST BOARD!

The address range for the Test board is 8000 to BFFF (hex) which is part of the same range as a 32 K Dynamic RAM board.
Nevertheless, the RAM board can be tested because the Test Monitor cancels the normal enabling of Bank 1 and Bank 2 on the RAM board.
By Selecting either Bank 1 or Bank 2, the Test Monitor can use the address range C000 to FFFF (hex) to access the selected bank.
The tests available on the Test Monitor menu are the following:
| Test no. | description |
| ---      | ---         |
| 1 | EAROM test |
| 2 | Video test |
| 3 | RAM test |
| 4 | Display Memory test |
| 5 | Marching test |
| 6 | Display test |
| 7 | Debugger |
| 8 | Interface test |
| 9 | Keyboard test |
| 0 | System test |
| A | Burn-in test |
| B | Program EAROM |
| C | Checksum test |
| T | Teletype mode |

The use of these tests are described in publ. no 5317, "TDV 2200 Test Monitor Specifications".

Test no. 7, Debugger contains several menus.
One of these; the Memory Layout Menu, can be used to change the contents of locations 50 to 5F (hex) in the non-volatile memory (EAROM).
This is explained in detail in the section [EAROM Programming](#earom-programming).

#### Installing the Test Board
1. Switch off the terminal.
1. Unscrew the two screws at the back of the terminal and slide the cabinet off.
1. Loosen the four screws along the upper edge of the chassis and lift off the perforated cover.
1. Unscrew the two upper screws in the upper edge of the perforated back cover and loosen the two screws at the lower edge. Then lift the cover off.
1. Enter the Test board onto the plastic spacers extending from the Mainboard, and plug the flat cable into connector W10 on the Mainboard. If the terminal has a Dynamic RAM board or Memory Module installed, plug the Test board into the vacant W10 connector on the Dynamic RAM board / Memory module.
1. Replace the perforated covers and switch on the terminal. When the Test Monitor Menu comes up, the Test board is ready for use.

## Replacing parts

### Picture tube

    WARNING! Unplug the power plug before opening the terminal!

1. Unscrew the two screws at the back, and remove the cabinet.
1. Loosen the two screws on both sides of the front cabinet, and remove the front.
1. Remove the perforated protective covers.
1. Pull the Tube board off the tube, and disconnect the EHT cable.
1. Loosen the deflection unit to prepare it for later removal from the neck of the tube.
1. Unscrew the four nuts holding the picture tube at the front. The tube can then be pulled out towards the front. Support the deflection unit with one hand while removing the tube.
1. Insert the new picture tube from the front and enter the deflection unit  onto it.
1. Before affixing the picture tube with the four nuts, replace the cabinet front temporarily and press it firmly against the chassis. If the cabinet front does not fit properly, remove or add washers between the chassis and the mounting brackets on the picture tube. Then remove the cabinet front, tighten the tube mounting nuts, and replace the front.
1. Replace the Tube board on the tube.
1. Reconnect the EHT cable to the picture tube and affix the cable under the cable clamp.
1. Replace the perforated protective covers and the cabinet.

NOTE: for adjustments, refer to the adjustment procedures described in part 7 of the TDV 2200 Service Manual, Power / Deflectin 1 & 2.

### Printed Circuit Boards

    WARNING! Never remove or replace printed circuit boards with the power switched on.

When replacing a board, arrange the cables so as to prevent them from being squeezed against metal parts.
In particular ensure that cables, W5, W6A and W6B are not jammed under the perforated protective cover.
Replace the cables in the plastic clamps.

    NOTE! Do not forget to reconnect the ground lead on the board.

### PROMs, ROMs, EAROM

    WARNING! Never remove or replace printed circuit boards with the power switched on.

Replacement of these components needs special attention since they are programmable devices, and must accordingly have the correct contents.
See section [Replacing or changing firmware](#replacing-or-changing-firmware).
Be cautious when inserting the circuit into the socket.
It is very easy to bend one or more of the pins so that they fail to make contact.
Bending of the pins may cause permanent damage to the circuit.

## Replacing or changing firmware
The firmware is unique for each model in the TDV 2200 series and is contained in two or more PROMs, one for the primitives, and one or two for the emulator (application program).

The primitives and the emulator are closely related and are always supplied in matched sets for replacement.

    WARNING! Never attempt to replace only the primitives PROM or only the emulator PROMs.

If the terminal is updated or modified to another version, the firmware must be replaced.
If this implies a change in the memory configuration, the contents of the EAROM (U43) must be changed accordingly to agree with the new configuration.

## EAROM programming
For this function a Test board must be installed (see [Test board](#test-board-1) section).
The proceed as follows:

### How to display the Required Menu
- When the Testmonitor comes up, type 7 to have the Debugger menu displayed.
- Depress MODE key twoice while holding the SHIFT key depressed. The terminal responds by displaying:
    Switch, PUSH-key or Layout change? (s/p/l)
- type l for layout change and the Memory Layout Menu comes up.

### How to use the Memory Layout Menu
This menu presents in plain text the memory layout as it is represented by the contents of EAROM locations 50 - 5F (hex).

Changing either one of the itms on this menu can be carried out from the keyboard, and will subsequently result in a corresponding change in the appropriate location of the EAROM.
- To change one particular item, move the cursor up to it and depress the ENTER key. Each time you press ENTER, a new paramater appears. Proceed until the desired one comes up and move the cursor to the next item to be changed.
- When all the desired changes have been made, depress ESC to return to the Debugger menu.
- Switch off the terminal and remove the Test board.
