# Keyboard
The keyboard is connected to the TDV 2200 terminal via a 6-wire cable through which signals are transmitted in both directions. Character codes representing depressed keys or status signals are sent to the terminal on serial form. In the oppsite direction commands from the terminal to set the operating mode or to control indicator lights are also transmitted on serial form.

The microprocessor in the keyboard performs the conversion from serial to parallel and vice versa, controls all the internal functions of the keyboard and the communication between keyboard and terminal.

## Signal mnemonics

| MNEMONIC    | FUNCTION |
| ---         | ---      |
| ALE        | Address latch enable |
| CARD CLOCK | Card reader clock |
| CARD DATA  | Card reader data line |
| CARD PRES  | Card present |
| CLICK ENBL | Enables click circuit |
| CRPCLK     | Card reader present clock |
| CR RDY LED | Lamp on the external card reader, indicating: |
|            | Card reader ready |
|            | Terminal waiting for card to be inserted |
| HOUSING    | Protective ground |
| INT        | Processor interrupt line (same as CARD CLOCK) |
| INTCLR     | Interrupt clear |
| PESEN      | Program store enable |
| RD         | Read; processor reads from data bus |
| REC        | Received data |
| TRANS      | Transmitted data |
| TRANS FLAG | Enables click circuit | 
| WR         | Write; processor writes on data bus |
