# Optional interface adapter - V.24

The original schematic shows that U3 pin 4 is connected to C3, and pin 5 to CT115 / J1 pin 17. 
Based on the datasheet for the SN75189, this must be an error. So I have switched pin 4 and pin 5 on U3 for this schematic.